# aws_local_dev_setup
Example to show how to setup AWS local development environment using golang aws-sdk-v2 and localstack.
In this example, there are 4 AWS components, Sqs, Sns, S3 and Lamda. When a message is published to Sns it will pass it to the subscribed Sqs quee and then the quee pass it to the subscribed Lamda function. Afterthat the Lambda function is invoked, it will process the message and create a result file in S3 bucket. All components are run locally without communicating with the external AWS services. Choosing SNS + SQS + Lamda approach over SNS + Lamda have some benefits and they are the followings
- Lambda is Reprocessing : When the Lambda fails to process certain event for some reason (e.g. timeout or lack of memory footprint), we can increase the timeout (to max 5 minutes) or memory (to max of 1.5GB) and restart our polling and we can reprocess the older events.
- It is also cost saving on Lamda invocations. With only Sns approach Lamda would get invonked every Sns event but with Sqs we can do batching with the messages sent to Lamda
  

## 1.Prerequisites
Need to install:
  - Docker and Docker Compose
  - AWS CLI v2
  - Golang v1.18


## 2.How to run
- Start docker compose:
```sh
docker-compose up
```

- After docker compose starts up,run the setup script to create S3 bucket, build and deploy lambda function:
```sh
sh local_setup.sh
```

- Run the following command to publish to sns topic: 
```sh
`aws --endpoint-url=http://localhost:4566 sns publish --topic-arn arn:aws:sns:us-east-1:000000000000:demo-topic --region us-east-1 --message '{"id": "0001", "name": "muzaffer"}'`
```

- Check if the file is created in S3: 
```sh
`aws --endpoint-url=http://localhost:4566 s3 ls s3://local-aws-demo/`
```

- Dowload files from S3: 
```sh
`aws --endpoint-url=http://localhost:4566 s3 sync s3://local-aws-demo/ .`
```

To stop the service and deleting volumes 
```sh
docker-compose down -v
```

 
 

