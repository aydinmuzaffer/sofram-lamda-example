#/bin/bash
set -x
S3_BUCKET="local-aws-demo"
LAMBDA_NAME="local-test"
QUEUE_NAME="demo-queue"
TOPIC_NAME="demo-topic"


echo "Creating Topic: $TOPIC_NAME"
awslocal --endpoint-url=http://localhost:4566  sns create-topic --name=${TOPIC_NAME}

echo "Creating Queue: $QUEUE_NAME"
awslocal --endpoint-url=http://localhost:4566  sqs create-queue --queue-name=${QUEUE_NAME}

echo "Subscribing Quee $QUEUE_NAME to Topic $TOPIC_NAME" 
SUBSCRIPTION_ARN=$(awslocal --endpoint-url=http://localhost:4566 sns subscribe \
                    --topic-arn arn:aws:sns:us-east-1:000000000000:${TOPIC_NAME} \
                    --protocol sqs \
                    --notification-endpoint http://localhost:4566/000000000000/${QUEUE_NAME} --output text)

echo "Setting  RawMessageDelivery true for  $SUBSCRIPTION_ARN" 
awslocal --endpoint-url http://localhost:4566 sns set-subscription-attributes \
        --subscription-arn "$SUBSCRIPTION_ARN" --attribute-name RawMessageDelivery --attribute-value true
                       

echo "Subscription Done; Quee $QUEUE_NAME to Topic $TOPIC_NAME With ARN $SUBSCRIPTION_ARN"

echo "Creating S3 bucket: $S3_BUCKET"
awslocal --endpoint-url=http://localhost:4566 s3 mb s3://${S3_BUCKET}  

echo "Building lambda deployment package"
GOOS=linux go build -o ./main .
zip deployment.zip main; rm main

echo "Deploying lambda"
awslocal --endpoint-url=http://localhost:4566 lambda create-function \
        --region us-east-1 \
        --function-name ${LAMBDA_NAME} \
        --runtime go1.x \
        --handler main \
        --memory-size 128 \
        --zip-file fileb://deployment.zip \
        --role arn:aws:iam::000000000000:role/irrelevant \
        --environment \
            "{\"Variables\":
                {
                    \"AWS_REGION\": \"us-east-1\",
                    \"AWS_ENDPOINT\": \"http://localstack:4566\",
                    \"S3_BUCKET\": \"${S3_BUCKET}\"
                }
            }"

echo "Subscribing Lamda to quee" 
LAMDA_SUBSCRIPTION_ARN = $(awslocal --endpoint-url=http://localhost:4566 lambda create-event-source-mapping \
                            --event-source-arn arn:aws:sqs:us-east-1:000000000000:${QUEUE_NAME} \
                            --function-name ${LAMBDA_NAME} \
                            --enabled \
                            --output text)
echo "Subscription Done; Quee $QUEUE_NAME to Function $LAMBDA_NAME With ARN $LAMDA_SUBSCRIPTION_ARN"
set +x